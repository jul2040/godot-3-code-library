extends Control
class_name TestGUI

enum {
	PROPERTY,
	MIN,
	MAX,
	PATH,
}
# this encodes all of the settings in the gui
func get_settings() -> Dictionary:
	return {}

func _ready():
	var settings:Dictionary = get_settings()
	for tab in settings.keys():
		var scroll_container = ScrollContainer.new()
		scroll_container.set_name(tab)
		var vbox = VBoxContainer.new()
		vbox.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		$TabContainer.add_child(scroll_container)
		scroll_container.add_child(vbox)
		for setting in settings[tab].keys():
			var entry:Array = settings[tab][setting]
			var slider = preload("TestSlider.tscn").instance()
			slider.set_display_name(setting)
			vbox.add_child(slider)
			slider.set_bounds(entry[MIN], entry[MAX])
			slider.set_value(get_node(entry[PATH]).get(entry[PROPERTY]))
			slider.set_path(entry[PATH])
			var r:float = entry[MAX]-entry[MIN]
			if(r <= 30.0):
				if(r < 0.1):
					slider.set_fine(2)
				else:
					slider.set_fine(1)
			slider.connect("changed", self, "on_slider_changed", [entry[PROPERTY], slider])

func on_slider_changed(value:float, property:String, slider:Node):
	get_node(slider.path).set(property, value)
