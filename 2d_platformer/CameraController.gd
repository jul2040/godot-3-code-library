extends Node

export var wait_for_floor = true

export var up_margin:float = 150
export var down_margin:float = 150

export var lookahead:float = 0.2

onready var camera:Camera2D = $Camera
onready var player = get_parent()

func _on_Player_touched_floor():
	if(wait_for_floor):
		camera.position.y = player.position.y

func _process(_delta):
	var lookahead_amount:float = player.velocity.x*lookahead
	camera.position.x = player.position.x+lookahead_amount
	if(not wait_for_floor):
		camera.position.y = player.position.y
	if(player.position.y > camera.position.y+down_margin):
		camera.position.y = player.position.y-down_margin
	elif(player.position.y+up_margin < camera.position.y):
		camera.position.y = player.position.y+up_margin
