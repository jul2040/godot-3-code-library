extends SpringArm

# because this is a child of the Damper3D, it will be damped like a spring

export var mouse_sensitivity:float = 0.005
export var min_distance:float = 0.0
export var max_distance:float = 10.0
export var zoom_interval:float = 1.0
export var zoom_time:float = 0.5

onready var damper:Damper3D = get_node("..")

func _ready():
	# dont interact with the player
	add_excluded_object(get_node("../..").get_rid())
	# capture the mouse
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	set_spring_length(target_length)

var mouse_motion:Vector2 = Vector2()
func _input(event:InputEvent):
	if(event is InputEventMouseMotion and Input.mouse_mode == Input.MOUSE_MODE_CAPTURED):
		mouse_motion += event.relative
	if(event.is_action_pressed("zoom_in")):
		set_spring_length(target_length-zoom_interval)
	if(event.is_action_pressed("zoom_out")):
		set_spring_length(target_length+zoom_interval)
	# pressing pause will toggle mouse capture
	if(event.is_action_pressed("pause")):
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED else Input.MOUSE_MODE_CAPTURED

# using a seperate variable for the target length
onready var target_length = spring_length
onready var start_height = translation.y
func set_spring_length(new:float):
	# make sure we cant zoom in or out too far
	new = clamp(new, min_distance, max_distance)
	target_length = new
	# if we are now in first person, damp less
	if(new > 0.0):
		# third person
		damper.movement_multiplier = Vector3(0.5, 1.0, 0.5)
	else:
		# first person
		damper.movement_multiplier = Vector3(0.1, 0.3, 0.1)
	# using a tween here instead of a damper because its more performant
	var tween = get_tree().create_tween()
	tween.set_ease(Tween.EASE_OUT)
	tween.set_trans(Tween.TRANS_BACK)
	# slowly zoom out over 0.5 seconds
	tween.tween_property(self, "spring_length", new, zoom_time)
	# move the camera up or down to match the zoom
	tween.parallel().tween_property(self, "translation:y", new*0.25+start_height, zoom_time)

func _process(_delta):
	# rotate based on mouse movement
	rotation.x -= mouse_motion.y*mouse_sensitivity
	rotation.y -= mouse_motion.x*mouse_sensitivity
	# reset the mouse motion
	mouse_motion = Vector2()
	# make sure we cant look up or down too far
	rotation.x = clamp(rotation.x, -PI/2, PI/2)
