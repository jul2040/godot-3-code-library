tool
extends ColorRect

export var shockwave_size:float setget set_shockwave_size

func set_shockwave_size(s:float):
	shockwave_size = s
	get_material().set_shader_param("size", s)
