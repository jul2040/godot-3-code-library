extends Node2D

# since this node is a child of the Damper2D, it will be damped

# display this node as a red dot
func _draw():
	draw_circle(Vector2(), 10, Color.red)
