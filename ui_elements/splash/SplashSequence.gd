extends Control

export var inbetween_time:float = 0.5
export var skippable:bool = true
export(Array, Texture) var images:Array
export var next_scene:PackedScene = null

signal finished

func _ready():
	var i:int = 0
	for image in images:
		var splash = preload("Splash.tscn").instance()
		splash.skippable = skippable
		splash.texture = image
		add_child(splash)
		yield(splash, "finished")
		if(i != len(images)-1):
			# only wait if this isnt the last splash
			yield(get_tree().create_timer(inbetween_time), "timeout")
		i += 1
	emit_signal("finished")
	if(next_scene == null):
		queue_free()
	else:
		TransitionManager.change_scene_to(next_scene)
