extends Node2D

# this scene is for testing the damper node

onready var damper:Damper2D = $Drag/Damper2D
onready var grapher:Grapher = $CanvasLayer/UI/Grapher

func _ready():
	# set the values of the sliders to the initial values of the damper
	$CanvasLayer/UI/F.value = damper.f
	$CanvasLayer/UI/Z.value = damper.z
	$CanvasLayer/UI/R.value = damper.r

func _on_F_value_changed(value):
	damper.f = value
	print_constants()
	grapher.update_graph(damper.f, damper.z, damper.r)

func _on_Z_value_changed(value):
	damper.z = value
	print_constants()
	grapher.update_graph(damper.f, damper.z, damper.r)

func _on_R_value_changed(value):
	damper.r = value
	print_constants()
	grapher.update_graph(damper.f, damper.z, damper.r)

func print_constants():
	print("F: %f\nZ: %f\nR: %f"%[damper.f, damper.z, damper.r])
