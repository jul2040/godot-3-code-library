extends Node
class_name Damper

# this is for dynamic damping of values
# if you know the start value, end value, and duration in advance, use a tween instead
# this node is for if you know the start value, but not the end value or duration

# inspired by this video: https://www.youtube.com/watch?v=KPoeNZZ6H4s

# natural resonant frequency (in Hertz)
# keep greater than 0
export var f:float = 10.0 setget set_f

func set_f(new):
	f = new
	compute_constants()

# damping coefficient
# 0 = no damping
# larger values = more damping
# keep greater than 0
export var z:float = 0.5 setget set_z

func set_z(new):
	z = new
	compute_constants()

# initial response
# negative values cause "undershoot"
# positive values cause "overshoot"
export var r:float = 0.0 setget set_r

func set_r(new):
	r = new
	compute_constants()

var k1:float
var k2:float
var k3:float
func compute_constants():
	k1 = z / (PI * f)
	k2 = 1 / (2 * PI * f)
	k3 = r * z / (2 * PI * f)

func _ready():
	compute_constants()

func max3(a:float, b:float, c:float):
	return max(a, max(b, c))

# using INF as a null value
var prev_input:float = INF
var output:float = INF
var output_d:float = 0.0
func update_damper(delta:float, input:float) -> float:
	if(prev_input == INF):
		prev_input = input
	if(output == INF):
		output = input
	var input_d = (input - prev_input) / delta
	prev_input = input
	var k2_stable:float = max3(k2, delta*delta/2 + delta*k1/2, delta*k1)
	output = output + delta*output_d
	output_d = output_d + delta * (input + k3*input_d - output - k1*output_d) / k2_stable
	return output

func reset():
	output_d = 0.0
	prev_input = INF
	output = INF
