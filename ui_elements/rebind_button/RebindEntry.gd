tool
extends Control

## this entry allows the user to rebind controls seperately for keyboard and controller.
## relies on the InputHelper and Saver singletons.

## the action to be rebound
export var action:String setget set_action

func set_action(new:String):
	action = new
	if(Engine.editor_hint):
		$HBoxContainer/KeyboardButton.action = action
		$HBoxContainer/ControllerButton.action = action
		return
	if(not is_inside_tree()):
		yield(self, "ready")
	keyboard_button.action = action
	keyboard_button.update_text()
	controller_button.action = action
	controller_button.update_text()

## the display name of the action
export var text:String setget set_text

func set_text(new:String):
	text = new
	if(not is_inside_tree()):
		yield(self, "ready")
	$HBoxContainer/Label.text = text

## whether the keyboard should have a rebind button
export var keyboard:bool = true
## whether the controller should have a rebind button
export var controller:bool = true

onready var keyboard_button:Button = $HBoxContainer/KeyboardButton
onready var controller_button:Button = $HBoxContainer/ControllerButton

func _ready():
	if(Engine.editor_hint):
		return
	if(not keyboard):
		keyboard_button.queue_free()
	if(not controller):
		keyboard_button.queue_free()
