extends HBoxContainer

signal changed(val)

var slider_name:String
func set_display_name(n:String):
	slider_name = n
	_on_HSlider_value_changed($HSlider.value)

func set_bounds(min_value:float, max_value:float):
	$HSlider.min_value = min_value
	$HSlider.max_value = max_value

func set_value(value:float):
	$HSlider.value = value
	_on_HSlider_value_changed($HSlider.value)

func _on_HSlider_value_changed(value):
	match fine:
		0:
			$Label.text = "%s: %.0f"%[slider_name, value]
		1:
			$Label.text = "%s: %.2f"%[slider_name, value]
		2:
			$Label.text = "%s: %.4f"%[slider_name, value]
	emit_signal("changed", value)

var path
func set_path(p):
	path = p

var fine:int = 0
func set_fine(f:int):
	fine = f
	match fine:
		0: $HSlider.step = 1
		1: $HSlider.step = 0.01
		2: $HSlider.step = 0.0001
	_on_HSlider_value_changed($HSlider.value)
